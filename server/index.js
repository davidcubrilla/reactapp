//don't forget to npm install nodeman
//then go to package.json and change the script
//nodeman allows you to run the server and make changes without restarting

const express = require('express');
const app = express();
const mongoose = require('mongoose');
const Player = require('./models/player');

//cors allows us to make api calls; networking stuff
const cors = require('cors');
app.use(cors());

app.use(express.json());

//connect to mongDB
const dbURI =
	'mongodb+srv://root:Mysqlsucks@fantasysoccerapp.9okbi.mongodb.net/SoccerDatabase?retryWrites=true&w=majority';
mongoose
	.connect(dbURI, { useNewUrlParser: true, useUnifiedTopology: true })
	//has to connect to db first before starting the server
	.then((result) => app.listen(3001))
	.catch((err) => console.log(err));

//method runs when you go to url
app.post('/add-player', (req, res) => {
	const playerName = req.body.playerName;
	const playerAge = req.body.playerAge;
	const playerTeam = req.body.playerTeam;
	const position = req.body.position;
	const country = req.body.country;

	const player = new Player({
		playerName: playerName,
		playerTeam: playerTeam,
		playerAge: playerAge,
		playerPosition: position,
		origin: country,
	});

	player
		.save()
		.then((result) => {
			res.send(result);
		})
		.catch((err) => {
			console.log(err);
		});
});

// app.get('/all-players', (req, res) => {
// 	Player.find()
// 		.then((result) => {
// 			res.send(result);
// 		})
// 		.catch((err) => {
// 			console.log(err);
// 		});
// });

// app.listen(3001, () => {
// 	console.log('Your server is running on port 3001');
// });
