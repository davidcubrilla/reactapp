const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const playerSchema = new Schema(
	{
		playerName: {
			type: String,
			required: true,
		},
		playerTeam: String,
		playerAge: Number,
		playerPosition: String,
		origin: String,
		stats: {
			goalsScored: Number,
			assist: Number,
			yellowCard: Number,
			redCard: Number,
			gamesPlayed: Number,
		},
	},
	{ timestamps: true }
);

//make it singular in name becuase it is going to pluralized it
const Player = mongoose.model('Player', playerSchema);

module.exports = Player;
