import React from 'react';
import Axios from 'axios';
import { Button, Container, Row } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

export class Form extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			playerName: '',
			playerAge: '',
			playerTeam: '',
			position: '',
			country: '',
		};

		//this.handleFirstNameChange = this.handleFirstNameChange.bind(this)
	}

	handlePlayerNameChange = (e) => {
		// we throw away the old state and replace with a new (immutable) state
		this.setState({ playerName: e.target.value });
	};

	handlePlayerAgeChange = (e) => {
		this.setState({ playerAge: e.target.value });
	};

	handlePlayerTeamChange = (e) => {
		this.setState({ playerTeam: e.target.value });
	};

	handlePositionChange = (e) => {
		this.setState({ position: e.target.value });
	};

	handleCountryChange = (e) => {
		this.setState({ country: e.target.value });
	};

	addPlayer() {
		Axios.post('http://localhost:3001/add-player', {
			playerName: this.state.playerName,
			playerAge: this.state.playerAge,
			playerTeam: this.state.playerTeam,
			position: this.state.position,
			country: this.state.country,
		}).then(() => {
			console.log('success');
		});
		window.location.reload();
	}

	render() {
		return (
			<Container fluid>
				<Row>
					<input
						type="text"
						name="playerName"
						placeholder="Player's Name"
						value={this.state.playerName}
						onChange={this.handlePlayerNameChange}
					/>
				</Row>
				<Row>
					<input
						type="text"
						name="playerAge"
						placeholder="Player's Age"
						value={this.state.playerAge}
						onChange={this.handlePlayerAgeChange}
					/>
				</Row>
				<Row>
					<input
						type="text"
						name="playerTeam"
						placeholder="Player's Team"
						value={this.state.playerTeam}
						onChange={this.handlePlayerTeamChange}
					/>
				</Row>
				<Row>
					<input
						type="text"
						name="position"
						placeholder="Position"
						value={this.state.position}
						onChange={this.handlePositionChange}
					/>
				</Row>
				<Row>
					<input
						type="text"
						name="country"
						placeholder="Country"
						value={this.state.country}
						onChange={this.handleCountryChange}
					/>
				</Row>
				<Row>
					<Button
						onClick={() => {
							this.addPlayer();
						}}
					>
						Submit
					</Button>
				</Row>
				<Row>
					<Button
						onClick={() => {
							window.location.reload();
						}}
					>
						Cancel
					</Button>
				</Row>
			</Container>
		);
	}
}
