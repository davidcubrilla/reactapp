import './App.css';
import React from 'react';
import { Form } from './components/form.js';
import { Results } from './components/result.js';

class App extends React.Component {
	constructor(props) {
		super(props);
		this.state = { apiResponse: '' };
	}

	//used with API folder

	// callAPI() {
	// 	fetch('http://localhost:9000/testAPI')
	// 		.then((res) => res.text())
	// 		.then((res) => this.setState({ apiResponse: res }));
	// }

	// componentWillMount() {
	// 	this.callAPI();
	// }

	render() {
		return (
			<div className="App">
				<Form />
				<Results />
				{/* <p>{this.state.apiResponse}</p> */}
			</div>
		);
	}
}

export default App;
